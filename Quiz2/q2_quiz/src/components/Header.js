
import { Link} from "react-router-dom";

import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';

 function Header() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>

        <Typography variant="h6">ยินดีต้อนรับสู่การสุ่มเลขคู่เเละคี่:</Typography>&nbsp;&nbsp;
        <Link to="/">Random</Link> 
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <Link to="/about">ผู้จัดทำ</Link>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
export default Header;
/*function Header(){


    return(
        <div align="left">
            ยินดีต้อนรับสู่การสุ่มเลขคู่เเละคี่: &nbsp;&nbsp;
                   <Link to="/">Random</Link> 
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <Link to="/about">ผู้จัดทำ</Link>
                   
            <hr/>
        </div>
    );
}
export default Header;*/