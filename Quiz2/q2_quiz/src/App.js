import logo from './logo.svg';
import './App.css';

import AboutUsPage from './pages/AboutUsPage';
import RandomPage from './pages/RandomPage';
import Header from './components/Header';

import {Routes, Route } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
       <Route path ="about" element={
                   <AboutUsPage />
                     }/>
      
         <Route path="/" element={
                      <RandomPage  number="/"/>
                    } />


        </Routes>
                   
    </div>
  );
}

export default App;
